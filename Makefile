# Makefile for dwmbar.

# Compiler.
CC = g++

# Build target.
TARGET = dwmbar

# Freetype defines.
FREETYPELIBS = -lfontconfig -lXft
FREETYPEINC = /usr/include/freetype2

# X11 defines.
X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

# Xinerama defines.
XINERAMALIBS  = -lXinerama
XINERAMAFLAGS = -DXINERAMA

# Includes, libraries, and macro expansions.
INCS = -I${X11INC} -I${FREETYPEINC} -I./src

LIBS = -L${X11LIB} -lX11 -lpthread ${XINERAMALIBS} ${FREETYPELIBS}

DEFS = ${XINERAMAFLAGS}

# Compile and linker flags.
CFLAGS   = -fPIC -std=c++17 -pedantic -Wall -Os ${DEFS} ${INCS}
LDFLAGS  = ${LIBS}

# Source and header files.
SRCDIR = src
SRC := $(shell cd $(SRCDIR); find . -name '*.cpp')
HDR := $(shell find . -name '*.h' -o -name '*.hpp')

# Object files.
OBJDIR = obj
OBJ = $(addprefix $(OBJDIR)/, ${SRC:.cpp=.o})


all: dwmbar

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp ${HDR}
	mkdir -p $(dir $@)
	${CC} -c ${CFLAGS} $< -o $@

dwmbar: ${OBJ}
	${CC} -o ${TARGET} ${OBJ} ${LDFLAGS}

clean:
	rm -rf ${TARGET} ${OBJDIR}/*

install:
	cp ${TARGET} /usr/bin
	chmod 0755 /usr/bin/${TARGET}

.PHONY: clean install
