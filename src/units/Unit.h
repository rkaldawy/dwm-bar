#ifndef UNIT_H
#define UNIT_H

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "draw/DrawableElement.h"
#include "palette/ColorScheme.h"
#include "palette/Palette.h"
#include "util/DrawUtil.h"
#include "util/Util.h"
#include "util/XIncludes.h"

class Unit
{
public:
    Unit(const std::shared_ptr<const ColorScheme>& scheme,
         const int& left_pad = 2,
         const int& right_pad = 6)
        : scheme_(scheme)
        , left_pad_(left_pad)
        , right_pad_(right_pad)
    {
        leading_text_ = "";
        leading_text_width_ = DrawUtils::get_text_width(leading_text_);
    }

    virtual ~Unit() {}

    // Update the unit.
    bool update(const int& x_right)
    {
        bool ret = refresh();
        x_ = x_right - get_width();
        update_child_elements();
        return ret;
    }

    // Update all the child drawables of the unit.
    virtual void update_child_elements() {}

    // Return true if the content of the unit has changed, and false otherwise.
    virtual bool refresh() = 0;

    // Will eventually be pure virtual.
    virtual void handle_button_event(XButtonPressedEvent* ev){};

    int get_x() { return x_; }

    int get_left_pad() { return left_pad_; }

    int get_width()
    {
        return leading_text_width_ + text_width_ + 2 * left_pad_ + right_pad_;
    }

    int get_leading_text_width() { return leading_text_width_ + left_pad_; }

    int get_text_width() { return text_width_ + left_pad_ + right_pad_; }

    // Get all drawable elements that the unit is responsible for.
    virtual const std::vector<std::shared_ptr<DrawableElt>> get_child_elements()
    {
        return {};
    }

protected:
    bool push_text(std::string& text)
    {
        if (text == text_)
        {
            return false;
        }
        text_ = text;
        text_width_ = DrawUtils::get_text_width(text_);
        return true;
    }

private:
    friend class Bar;

    // The unit's color scheme.
    std::shared_ptr<const ColorScheme> scheme_;

    // x-position of the unit.
    int x_;

    int left_pad_;

    int right_pad_;

    // Any leading patterns before the text.
    std::string leading_text_;

    // Width of the leading text.
    int leading_text_width_;

    // The text to be displayed by the unit.
    std::string text_;

    // Width of the text.
    int text_width_;
};

#endif
