#ifndef VOLUME_UNIT_H
#define VOLUME_UNIT_H

#include "units/Unit.h"

static std::string VOLUME_MUTE = "🔇";
static std::string VOLUME_LOW = "🔈";
static std::string VOLUME_MEDIUM = "🔉";
static std::string VOLUME_HIGH = "🔊";

static std::string VOLUME_SYMBOLS[] = {
    VOLUME_LOW,
    VOLUME_MEDIUM,
    VOLUME_HIGH,
};

class VolumeUnit : public Unit
{
public:
    VolumeUnit(std::shared_ptr<const ColorScheme> scheme)
        : Unit(scheme)
    {}

    virtual ~VolumeUnit() {}

    virtual bool refresh()
    {
        // std::string vol_raw = exec("python3
        // /home/rkaldawy/Documents/scripts/dwm/get_volume.py");

        std::string volume_str = exec("pamixer --get-volume");
        int volume = std::stoi(volume_str);

        std::string muted_str = exec("pamixer --get-mute");
        bool muted = false;
        if (muted_str == "true")
        {
            muted = true;
        }

        std::string symbol;
        if (volume <= 0 || muted)
        {
            symbol = VOLUME_MUTE;
        }
        else if (volume <= 33)
        {
            symbol = VOLUME_LOW;
        }
        else if (volume <= 66)
        {
            symbol = VOLUME_MEDIUM;
        }
        else
        {
            symbol = VOLUME_HIGH;
        }

        std::string text =
            " " + symbol + " " + int_to_padded_str(volume, 2) + "%";
        return push_text(text);
    }
};

#endif
