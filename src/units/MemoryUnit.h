#ifndef MEMORY_UNIT_H
#define MEMORY_UNIT_H

#include "units/Unit.h"

static std::string MEMORY_SYMBOL = "";

/* Reports current memory usage. */
class MemoryUnit : public Unit
{
public:
    MemoryUnit(std::shared_ptr<const ColorScheme> scheme)
        : Unit(scheme)
    {}

    virtual ~MemoryUnit() {}

    virtual bool refresh()
    {
        std::string mem_raw = exec("free");
        double total = std::stoi(split(split(mem_raw, "Mem:")[1], " ")[0]);
        double used = std::stoi(split(split(mem_raw, "Mem:")[1], " ")[1]);
        int percent = (used / total) * 100.0;

        std::string text = " " + MEMORY_SYMBOL + std::to_string(percent) + "%";
        return push_text(text);
    }
};

#endif
