#ifndef CPU_UNIT_HPP
#define CPU_UNIT_HPP

#include "units/Unit.h"

static std::string CPU_SYMBOL = "";

/* Reports current CPU usage. */
class CpuUnit : public Unit
{
public:
    CpuUnit(std::shared_ptr<const ColorScheme> scheme, int refresh_period)
        : Unit(scheme)
        , refresh_period_(refresh_period)
    {
        initialized_ = false;
        auto metric = get_memory_metric();
        previous_idle_ = metric.first;
        previous_total_ = metric.second;
        last_drawn_ = std::chrono::system_clock::now();
    }

    virtual ~CpuUnit() {}

    virtual bool refresh()
    {
        auto now = std::chrono::system_clock::now();
        auto delta = std::chrono::duration_cast<std::chrono::microseconds>(
                         now - last_drawn_)
                         .count();

        if (delta < refresh_period_ && initialized_)
        {
            return false;
        }

        last_drawn_ = std::chrono::system_clock::now();

        auto metric = get_memory_metric();
        int idle = metric.first;
        int total = metric.second;
        int cpu_percent =
            percent_difference(idle - previous_idle_, total - previous_total_);
        previous_idle_ = idle;
        previous_total_ = total;

        std::string text =
            " " + CPU_SYMBOL + int_to_padded_str(cpu_percent, 2) + "%";

        initialized_ = true;
        return push_text(text);
    }

private:
    std::pair<int, int> get_memory_metric()
    {
        std::string mem_raw = split(exec("cat /proc/stat"), "cpu")[0];
        std::vector<std::string> cycles = split(mem_raw, " ");
        int idle = std::stoi(cycles[3]);

        int total = 0;
        for (auto& cycle : cycles)
        {
            total += std::stoi(cycle);
        }

        return std::make_pair(idle, total);
    }

    double percent_difference(int a, int total)
    {
        return ((total - a) / static_cast<double>(total)) * 100;
    }

    int previous_idle_;

    int previous_total_;

    int refresh_period_;

    bool initialized_;

    std::chrono::system_clock::time_point last_drawn_;
};

#endif
