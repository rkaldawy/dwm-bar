#ifndef WIFI_UNIT_H
#define WIFI_UNIT_H

#include <iostream>
#include <string>

#include "units/Unit.h"
#include "util/Util.h"

static std::string WIFI_SYMBOL = "🛜";

/* Wifi utility. */
class WifiUnit : public Unit
{
public:
    WifiUnit(std::shared_ptr<const ColorScheme> scheme)
        : Unit(scheme)
    {}

    virtual ~WifiUnit() {}

    virtual bool refresh()
    {
        std::string network = exec("nmcli -t -f name connection show --active");
        std::string device =
            exec("nmcli -t -f device connection show --active");

        network = split(trim(network), "\n")[0];
        device = split(trim(device), "\n")[0];

        std::string text = WIFI_SYMBOL + " " + network + " on " + device;

        return push_text(text);
    }
};

#endif
