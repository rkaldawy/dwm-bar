#ifndef DATE_UNIT_H
#define DATE_UNIT_H

#include <ctime>
#include <iostream>
#include <stdio.h>
#include <string>

#include "units/Unit.h"

static std::string CALENDAR_ICON = "📅";
static std::string MONTH_ARRAY[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                     "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

/* Displays the current battery level. */
class DateUnit : public Unit
{
public:
    DateUnit(std::shared_ptr<const ColorScheme> scheme)
        : Unit(scheme)
    {}

    virtual ~DateUnit() {}

    virtual bool refresh()
    {
        time_t t = time(NULL);
        tm* timePtr = localtime(&t);

        std::string date = "";
        date += MONTH_ARRAY[timePtr->tm_mon] + " ";
        date += std::to_string(timePtr->tm_mday) + " ";
        date += std::to_string(timePtr->tm_year + 1900);

        std::string text = " " + CALENDAR_ICON + " " + date;
        return push_text(text);
    }
};

#endif