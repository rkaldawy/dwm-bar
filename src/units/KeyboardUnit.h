#ifndef KEYBOARD_UTIL_H
#define KEYBOARD_UTIL_H

#include "units/Unit.h"

static std::string KEYBOARD_SYMBOL = "⌨";

class KeyboardUnit : public Unit
{
public:
    KeyboardUnit(std::shared_ptr<const ColorScheme> scheme)
        : Unit(scheme)
    {}

    virtual ~KeyboardUnit() {}

    virtual bool refresh()
    {
        std::string text = " " + KEYBOARD_SYMBOL + " ";

        std::string command = exec("ibus engine");
        if (command.empty())
        {
            return push_text(text);
        }

        std::string layout = split(command, "\x0a")[0];
        auto it = languages_.find(layout);
        if (it != languages_.end())
        {
            text += it->second;
        }
        else
        {
            text += "English";
        }

        return push_text(text);
    }

private:
    std::unordered_map<std::string, std::string> languages_ = {
        { "mozc-jp", "日本語" },
    };
};

#endif
