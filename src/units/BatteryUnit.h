#ifndef BATTERY_UNIT_H
#define BATTERY_UNIT_H

#include "units/Unit.h"

static std::string EMPTY_BATTERY = "";
static std::string LOW_BATTERY = "";
static std::string MEDIUM_BATTERY = "";
static std::string HIGH_BATTERY = "";
static std::string FULL_BATTERY = "";

static std::string BATTERY_SYMBOLS[] = { EMPTY_BATTERY,
                                         LOW_BATTERY,
                                         MEDIUM_BATTERY,
                                         HIGH_BATTERY,
                                         FULL_BATTERY };

static std::string VOLTAGE_SYMBOL = "⚡";

/* Displays the current battery level. */
class BatteryUnit : public Unit
{
public:
    BatteryUnit(std::shared_ptr<const ColorScheme> scheme, int refresh_period)
        : Unit(scheme)
        , refresh_period_(refresh_period)
    {
        charging_index_ = 0;
        last_drawn_ = std::chrono::system_clock::now();
    }

    virtual ~BatteryUnit() {}

    virtual bool refresh()
    {
        std::string raw = exec("acpi");
        int level = std::stoi(split(split(raw, "%")[0], " ").back());
        std::string state = split(split(raw, " ")[2], ",")[0];

        std::string icon = lookup_batsym(state, level);
        std::string text = " " + icon + " " + std::to_string(level) + "%";

        return push_text(text);
    }

private:
    std::string lookup_batsym(std::string state, int level)
    {
        if (level >= 100)
        {
            return FULL_BATTERY;
        }

        auto is_charging = [&]() {
            return state == "Unknown" || state == "Charging";
        };

        if (is_charging())
        {
            std::string ret =
                VOLTAGE_SYMBOL + " " + BATTERY_SYMBOLS[charging_index_];

            auto now = std::chrono::system_clock::now();
            auto delta = static_cast<uint64_t>(
                std::chrono::duration_cast<std::chrono::microseconds>(
                    now - last_drawn_)
                    .count());

            if (delta > refresh_period_)
            {
                charging_index_ = (charging_index_ + 1) % 5;
                last_drawn_ = std::chrono::system_clock::now();
            }

            return ret;
        }
        else
        {
            if (level < 10)
            {
                return EMPTY_BATTERY;
            }
            else if (level >= 90)
            {
                return FULL_BATTERY;
            }
            else
            {
                return BATTERY_SYMBOLS[(level / 25) + 1];
            }
        }
    }

    uint32_t charging_index_;

    uint64_t refresh_period_;

    std::chrono::system_clock::time_point last_drawn_;
};

#endif
