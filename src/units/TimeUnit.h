#ifndef TIME_UNIT_H
#define TIME_UNIT_H

#include <chrono>
#include <ctime>
#include <locale>
#include <string>

#include "units/Unit.h"

std::string ONE_OCLOCK = "\xf0\x9f\x95\x90";
std::string ONE_THIRTY = "🕜";
std::string TWO_OCLOCK = "🕑";
std::string TWO_THIRTY = "🕝";
std::string THREE_OCLOCK = "🕒";
std::string THREE_THIRTY = "🕞";
std::string FOUR_OCLOCK = "🕓";
std::string FOUR_THIRTY = "🕟";
std::string FIVE_OCLOCK = "🕔";
std::string FIVE_THIRTY = "🕠";
std::string SIX_OCLOCK = "🕕";
std::string SIX_THIRTY = "🕡";
std::string SEVEN_OCLOCK = "🕖";
std::string SEVEN_THIRTY = "🕢";
std::string EIGHT_OCLOCK = "🕗";
std::string EIGHT_THIRTY = "🕣";
std::string NINE_OCLOCK = "🕘";
std::string NINE_THIRTY = "🕤";
std::string TEN_OCLOCK = "🕙";
std::string TEN_THIRTY = "🕥";
std::string ELEVEN_OCLOCK = "🕚";
std::string ELEVEN_THIRTY = "🕦";
std::string TWELVE_OCLOCK = "🕛";
std::string TWELVE_THIRTY = "🕧";

std::string CLOCK_SYMBOLS[] = {
    TWELVE_OCLOCK, TWELVE_THIRTY, ONE_OCLOCK,    ONE_THIRTY,   TWO_OCLOCK,
    TWO_THIRTY,    THREE_OCLOCK,  THREE_THIRTY,  FOUR_OCLOCK,  FOUR_THIRTY,
    FIVE_OCLOCK,   FIVE_THIRTY,   SIX_OCLOCK,    SIX_THIRTY,   SEVEN_OCLOCK,
    SEVEN_THIRTY,  EIGHT_OCLOCK,  EIGHT_THIRTY,  NINE_OCLOCK,  NINE_THIRTY,
    TEN_OCLOCK,    TEN_THIRTY,    ELEVEN_OCLOCK, ELEVEN_THIRTY
};

/* Displays the current time. */
class TimeUnit : public Unit
{
public:
    /* Constructor. */
    TimeUnit(std::shared_ptr<const ColorScheme> scheme, bool military = false)
        : Unit(scheme)
    {
        military_ = military;
    }

    /* Destructor. */
    virtual ~TimeUnit() {}

    virtual bool refresh()
    {
        auto ct = std::chrono::system_clock::now();
        std::time_t time = std::chrono::system_clock::to_time_t(ct);
        std::string time_str = std::ctime(&time);
        time_str = split(time_str, " ")[3];

        int hour_24 = std::stoi(split(time_str, ":")[0]);
        int hour_12 = hour_24 % 12;
        if (hour_12 == 0)
        {
            hour_12 = 12;
        }
        int minute = std::stoi(split(time_str, ":")[1]);

        std::string text = " ";
        text += get_clock_symbol(hour_24, minute);
        text += " ";

        if (military_ && hour_24 < 10)
        {
            text += "0";
        }
        text += military_ ? std::to_string(hour_24) : std::to_string(hour_12);
        text += ":" + int_to_padded_str(minute, 2, "0");
        if (!military_)
        {
            text += " ";
            text += hour_24 < 12 ? "AM" : "PM";
        }

        return push_text(text);
    }

    virtual void handle_button_event(XButtonPressedEvent* ev) override
    {
        military_ = !military_;
    };

private:
    std::string get_clock_symbol(const int& hour, const int& min)
    {
        uint32_t utime = ((hour % 12) * 60 + min) / 30;
        return CLOCK_SYMBOLS[utime];
    }

    bool military_;
};

#endif
