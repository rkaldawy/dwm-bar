#ifndef POWER_UNIT_H
#define POWER_UNIT_H

#include "menus/Menu.h"
#include "units/Unit.h"

std::string POWER_SYMBOL = "⏻";
// std::string LOCK_SYMBOL = "🔒";
std::string LOCK_SYMBOL = "🔐";
std::string SLEEP_SYMBOL = "⏾";
std::string RESET_SYMBOL = "🗘";
std::string SHUTDOWN_SYMBOL = "⏻";

class PowerUnit : public Unit
{
public:
    PowerUnit(std::shared_ptr<const ColorScheme> scheme,
              std::shared_ptr<const ColorScheme> menu_scheme)
        : Unit(scheme, 10)
    {
        std::string text = "⏼";
        push_text(text);

        MenuItem suspend_item(
            SLEEP_SYMBOL,
            menu_scheme,
            []() { exec("systemctl suspend"); },
            1,
            1);
        MenuItem reboot_item(
            RESET_SYMBOL, menu_scheme, []() { exec("reboot"); }, 3);
        MenuItem power_item(
            POWER_SYMBOL, menu_scheme, []() { exec("poweroff"); }, 1, 1);
        std::vector<MenuItem> menu_items = {
            suspend_item,
            reboot_item,
            power_item,
        };

        power_menu_ = std::make_shared<Menu>(1900, 18, scheme, menu_items);
    }

    virtual ~PowerUnit() {}

    virtual bool refresh() override { return false; }

    virtual void update_child_elements() override
    {
        power_menu_->resize(get_x() + get_leading_text_width(),
                            get_text_width());
    }

    virtual void handle_button_event(XButtonPressedEvent* ev) override
    {
        power_menu_->set_should_draw(!power_menu_->should_draw());
    }

    virtual const std::vector<std::shared_ptr<DrawableElt>> get_child_elements()
        override
    {
        return { power_menu_ };
    }

private:
    std::shared_ptr<Menu> power_menu_;
};

#endif