#include "manager/Manager.h"
#include "util/XIncludes.h"

int main()
{
    Display* dpy = XOpenDisplay(NULL);

    DrawManager::get_instance().setup(dpy);
    DrawManager::get_instance().run();

    XEvent ev;
    XSync(dpy, False);

    while (true)
    {
        XNextEvent(dpy, &ev);
        DrawManager::get_instance().push_event(&ev);
    }
}
