#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <map>
#include <memory>
#include <queue>
#include <vector>

#include "palette/ColorScheme.h"
#include "util/DrawUtil.h"
#include "util/Util.h"
#include "util/XIncludes.h"

class DrawableElt
{
public:
    DrawableElt(std::string name);

    virtual ~DrawableElt() {}

    // How do we draw the drawable?
    virtual void draw();

    // How do we erase the drawable?
    virtual void clear();

    // Note: initial event handling done by the DrawManager
    virtual bool handle_button_event(XEvent* e);

    // Returns the list of drawables that this drawable is responsible for.
    virtual std::vector<std::shared_ptr<DrawableElt>> get_child_elements();

    // Updates the position of the drawable on the screen.
    void update_position(const int& x, const int& y);

    // Is the drawable visible?
    bool is_visible();

    // Is the drawable currently being drawn?
    bool is_drawing();

    // Should the drawable be visible?
    bool should_draw();

    // Set whether the drawable should be visible or not.
    void set_should_draw(bool should_draw);

protected:
    void instantiate_window();

    Window construct_window(bool define_cursor);

    void resize_window();

    int x_;

    int y_;

    int width_;

    int height_;

    int left_pad_;

    int right_pad_;

    // Size and position of the drawable screen.
    int monitor_width_;

    // The name of the window.
    std::string name_;

    // The window holding the drawable.
    Window window_;

    // Default scheme of the drawable.
    std::shared_ptr<const ColorScheme> default_scheme_;

    // Flag set if object should be visible.
    bool should_draw_;

    // Flag set while object is being drawn.
    bool drawing_;

    // Flag set when object is visible.
    bool visible_;
};

#endif
