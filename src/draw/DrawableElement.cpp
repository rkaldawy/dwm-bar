#include "draw/DrawableElement.h"
#include "manager/Manager.h"
#include "palette/Palette.h"

DrawableElt::DrawableElt(std::string name)
    : monitor_width_(sw)
    , name_(name)
{
    visible_ = false;
}

Window DrawableElt::construct_window(bool define_cursor)
{
    XSetWindowAttributes wa;
    wa.background_pixmap = ParentRelative;
    wa.event_mask = ButtonPressMask | ExposureMask;
    wa.override_redirect = True;

    char* name = strdup(name_.c_str());
    XClassHint ch = { name, name };

    Window window =
        XCreateWindow(display,
                      root,
                      x_,
                      y_,
                      width_,
                      height_,
                      0,
                      DefaultDepth(display, screen),
                      CopyFromParent,
                      DefaultVisual(display, screen),
                      CWOverrideRedirect | CWBackPixmap | CWEventMask,
                      &wa);

    if (define_cursor)
    {
        XDefineCursor(display, window, XCreateFontCursor(display, XC_left_ptr));
    }

    XTextProperty window_name;
    char* name_list[] = { name };
    XStringListToTextProperty(name_list, 1, &window_name);
    XSetWMName(display, window, &window_name);

    XMapRaised(display, window);
    XSetClassHint(display, window, &ch);

    free(name);
    return window;
}

void DrawableElt::resize_window()
{
    XMoveResizeWindow(display, window_, x_, y_, width_, height_);
}

void DrawableElt::instantiate_window()
{
    window_ = construct_window(true);
    DrawUtils::map_to_window(window_, 0, 0, sw, sh);
    visible_ = true;
}

void DrawableElt::draw()
{
    return;
}

void DrawableElt::clear()
{
    if (visible_)
    {
        XDestroyWindow(display, window_);
    }
    visible_ = false;
}

bool DrawableElt::handle_button_event(XEvent* e)
{
    return false;
}

std::vector<std::shared_ptr<DrawableElt>> DrawableElt::get_child_elements()
{
    return {};
}

void DrawableElt::update_position(const int& x, const int& y)
{
    x_ = x;
    y_ = y;
}

bool DrawableElt::is_visible()
{
    return visible_;
}

bool DrawableElt::is_drawing()
{
    return drawing_;
}

bool DrawableElt::should_draw()
{
    return should_draw_;
}

void DrawableElt::set_should_draw(bool should_draw)
{
    should_draw_ = should_draw;
}
