#ifndef DRAW_MANAGER_H
#define DRAW_MANAGER_H

#include <memory>
#include <mutex>
#include <thread>

#include "bar/Bar.h"
#include "draw/DrawableElement.h"
#include "palette/Palette.h"
#include "units/DateUnit.h"
#include "util/XIncludes.h"

/**
 * This is the head class for dwmbar. All drawable objects are managed by this
 * class. All exposed API calls are serviced by this class.
 */
class DrawManager
{
public:
    static DrawManager& get_instance();

    DrawManager(const DrawManager&) = delete;

    DrawManager& operator=(const DrawManager&) = delete;

    virtual ~DrawManager() {}

    static const int REFRESH_RATE = 3000;

    void push_event(XEvent* event);

    /* todo: move the event handler over to a handling thread in this class */
    void handle_event();

    void handle_button_event(XEvent* ev);

    int setup(Display* dpy);

    void run();

    int get_bar_size();

private:
    DrawManager();

    void register_colors();

    void register_schemes();

    void register_fonts();

    void draw_loop();

    // Tracks whether or not dwmbar has been set up yet.
    bool exists_;

    std::thread draw_thread_;

    // Mutex for interfacing with the bar.
    std::mutex bar_lock_;

    // How often dwmbar refreshes itself.
    uint64_t refresh_period_;

    // A reference to the main dwmbar.
    std::unique_ptr<Bar> bar_;

    // List of all other objects which can be drawn.
    std::vector<std::shared_ptr<DrawableElt>> drawables_;

    // Queue of incoming mouse and keyboard events.
    std::queue<XEvent*> event_queue_;
};

#endif
