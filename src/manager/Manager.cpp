#include <chrono>

#include "manager/Manager.h"

/* The display and screen, to be used for the entire program. */
Display* display = NULL;
int screen = 0;
Window root = 0;
int sw, sh = 0;

DrawManager& DrawManager::get_instance()
{
    static DrawManager draw_manager;
    return draw_manager;
}

DrawManager::DrawManager()
{
    refresh_period_ = 100000; // 100 ms.
    exists_ = false;
}

void DrawManager::push_event(XEvent* event)
{

    auto event_copy = new XEvent;
    memcpy(event_copy, event, sizeof(XEvent));
    bar_lock_.lock();
    event_queue_.push(event_copy);
    bar_lock_.unlock();
}

void DrawManager::handle_event()
{
    if (event_queue_.empty())
    {
        return;
    }

    XEvent* ev = event_queue_.front();

    // todo: document magic numbers
    switch (ev->type)
    {
        case ButtonPress:
        case 14:
        case 16:
        case 28:
            handle_button_event(ev);
            break;
        // todo: handle key presses
        default:
            break;
    }

    free(ev);
    event_queue_.pop();
}

void DrawManager::handle_button_event(XEvent* ev)
{
    bool ret = false;
    ret |= bar_->handle_button_event(ev);
    if (ret)
    {
        return;
    }

    for (auto& drawable : drawables_)
    {
        ret |= drawable->handle_button_event(ev);
        if (ret)
        {
            return;
        }
    }
}

int DrawManager::get_bar_size()
{
    int ret;
    bar_lock_.lock();
    ret = bar_->get_x();
    bar_lock_.unlock();
    return ret;
}

int DrawManager::setup(Display* dpy)
{
    if (exists_)
    {
        return -1;
    }

    display = dpy;
    screen = DefaultScreen(display);
    root = RootWindow(display, screen);
    sw = DisplayWidth(display, screen);
    sh = DisplayHeight(display, screen);

    register_schemes();
    register_fonts();

    bar_ = std::make_unique<Bar>();

    drawables_ = bar_->get_child_elements();

    exists_ = true;
    return 0;
}

void DrawManager::run()
{
    if (!exists_)
    {
        printf("Error: Dwmbar has not been instantiated. Please call "
               "DWMBAR_setupbar()\n");
        return;
    }

    draw_thread_ = std::thread([this] { this->draw_loop(); });
}

void DrawManager::draw_loop()
{
    while (true)
    {
        auto start = std::chrono::system_clock::now();

        bar_lock_.lock();

        handle_event();
        bar_->draw();
        for (auto& drawable : drawables_)
        {
            if (drawable->should_draw() || drawable->is_drawing())
            {
                drawable->draw();
            }
            if (drawable->is_visible() && !drawable->should_draw())
            {
                drawable->clear();
            }
        }

        bar_lock_.unlock();

        auto end = std::chrono::system_clock::now();
        auto delta = static_cast<uint64_t>(
            std::chrono::duration_cast<std::chrono::microseconds>(end - start)
                .count());
        // printf("%d ms elapsed.\n", delta / 1000);
        if (delta > refresh_period_)
        {
            printf("WARNING: Refresh rate has slipped.\n");
        }
        else
        {
            usleep(refresh_period_ - delta);
        }
    }
}

void DrawManager::register_colors()
{
    Palette::get_instance().add_color("white", "#ffffff");
    Palette::get_instance().add_color("gray3", "#777777");
    Palette::get_instance().add_color("black", "#000000");

    Palette::get_instance().add_color("green", "#00ff04");
    Palette::get_instance().add_color("red", "#bb3333");

    Palette::get_instance().add_color("lblue", "#528fbb");
    Palette::get_instance().add_color("blue", "#3333bb");
    Palette::get_instance().add_color("dblue", "#256b8b");
    Palette::get_instance().add_color("sky_blue", "#b3d4f3");
    Palette::get_instance().add_color("lime", "#d4e5ca");

    Palette::get_instance().add_color("lpink", "#f7ced9");
    Palette::get_instance().add_color("dpink", "#e5adc6");
}

void DrawManager::register_schemes()
{
    register_colors();

    // Lime scheme.
    Palette::get_instance().add_scheme("lime", "black", "lime", "lime");
    // Dark pink scheme.
    Palette::get_instance().add_scheme("dpink", "black", "dpink", "dpink");
    // Light pink scheme.
    Palette::get_instance().add_scheme("lpink", "black", "lpink", "lpink");
    // Light blue scheme.
    Palette::get_instance().add_scheme("lblue", "white", "lblue", "lblue");
    // Dark blue scheme.
    Palette::get_instance().add_scheme("dblue", "white", "dblue", "dblue");
    // Sky blue scheme.
    Palette::get_instance().add_scheme(
        "sky_blue", "white", "sky_blue", "sky_blue");
    // Black scheme.
    Palette::get_instance().add_scheme("black", "white", "gray3", "gray3");

    // Special dark blue scheme, for now.
    Palette::get_instance().add_scheme("dblue2", "green", "dblue", "dblue");
}

void DrawManager::register_fonts()
{
    std::vector<std::string> fonts{
        "Terminus:size=12",
        "xos4 Terminess Powerline:size=12:antialias=true",
        "NotoSans Nerd Font:style=Regular:size=12:antialias=true",
        "Siji:style=Regular:size=12:antialias=true",
    };
    Palette::get_instance().create_fontset(fonts);
}
