#ifndef X_INCLUDES_H
#define X_INCLUDES_H

#include <X11/Xatom.h>
#include <X11/Xft/Xft.h>
#include <X11/Xlib.h>
#include <X11/Xproto.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>
#ifdef XINERAMA
#include <X11/extensions/Xinerama.h>
#endif /* XINERAMA */

/* The display and screen is shared for the entire program. */
extern Display* display;
extern int screen;
extern Window root;
extern int sw, sh;

#endif
