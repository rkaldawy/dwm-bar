#ifndef UTIL_HPP
#define UTIL_HPP

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#define MAX(A, B) ((A) > (B) ? (A) : (B))
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define BETWEEN(X, A, B) ((A) <= (X) && (X) <= (B))

#define LENGTH(X) (sizeof X / sizeof X[0])

void die(const char* fmt, ...);
std::vector<std::string> split(const std::string& str,
                               const std::string& delim);
std::string trim(std::string& str);
char* concat(const char* s1, const char* s2);

std::string exec(const char* cmd);

void LOG_PRINT(const char* fmt, ...);

std::string int_to_padded_str(const int& num,
                              const int& pad_length,
                              std::string pad = " ");

#endif
