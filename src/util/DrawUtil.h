#ifndef DRW_HPP
#define DRW_HPP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "palette/ColorScheme.h"
#include "palette/Font.h"
#include "util/Util.h"
#include "util/XIncludes.h"

#include <iostream>
#include <vector>

namespace DrawUtils {

int draw_text(const std::string& text,
              const std::shared_ptr<const ColorScheme>& scheme,
              int32_t x,
              int32_t y,
              uint32_t width,
              uint32_t height,
              uint32_t lpad,
              bool invert);

size_t get_text_width(const std::string& text);

void draw_rectangle(const std::shared_ptr<const ColorScheme>& scheme,
                    int32_t x,
                    uint32_t y,
                    uint32_t width,
                    uint32_t height,
                    bool filled,
                    bool invert);

void map_to_window(Window window,
                   const int& x,
                   const int& y,
                   const unsigned int& width,
                   const unsigned int& height);

}

#endif
