#include "util/DrawUtil.h"
#include "palette/Palette.h"

namespace DrawUtils {

#define FOREGROUND_COLOR(scheme_ptr, i)                                        \
    (*scheme_ptr)[invert ? COLOR_BG : COLOR_FG]

#define BACKGROUND_COLOR(scheme_ptr, invert)                                   \
    (*scheme_ptr)[invert ? COLOR_FG : COLOR_BG]

#define UTF_INVALID 0xFFFD
#define UTF_SIZE 4

static const unsigned char utfbyte[UTF_SIZE + 1] = { 0x80,
                                                     0,
                                                     0xC0,
                                                     0xE0,
                                                     0xF0 };

static const unsigned char utfmask[UTF_SIZE + 1] = { 0xC0,
                                                     0x80,
                                                     0xE0,
                                                     0xF0,
                                                     0xF8 };

static const long utfmin[UTF_SIZE + 1] = { 0, 0, 0x80, 0x800, 0x10000 };

static const long utfmax[UTF_SIZE + 1] = { 0x10FFFF,
                                           0x7F,
                                           0x7FF,
                                           0xFFFF,
                                           0x10FFFF };

long utf8decodebyte(const char c, size_t* i)
{
    for (*i = 0; *i < (UTF_SIZE + 1); ++(*i))
        if (((unsigned char)c & utfmask[*i]) == utfbyte[*i])
            return (unsigned char)c & ~utfmask[*i];
    return 0;
}

size_t utf8validate(long* u, size_t i)
{
    if (!BETWEEN(*u, utfmin[i], utfmax[i]) || BETWEEN(*u, 0xD800, 0xDFFF))
        *u = UTF_INVALID;
    for (i = 1; *u > utfmax[i]; ++i)
        ;
    return i;
}

size_t utf8decode(const char* c, long* u, size_t clen)
{
    size_t i, j, len, type;
    long udecoded;

    *u = UTF_INVALID;
    if (!clen)
        return 0;
    udecoded = utf8decodebyte(c[0], &len);
    if (!BETWEEN(len, 1, UTF_SIZE))
        return 1;
    for (i = 1, j = 1; i < clen && j < len; ++i, ++j)
    {
        udecoded = (udecoded << 6) | utf8decodebyte(c[i], &type);
        if (type)
            return j;
    }
    if (j < len)
        return 0;
    *u = udecoded;
    utf8validate(u, len);

    return len;
}

class DrawAgent
{
public:
    static DrawAgent& get_instance()
    {
        static DrawAgent draw_agent;
        return draw_agent;
    }

    DrawAgent(const DrawAgent&) = delete;

    DrawAgent& operator=(const DrawAgent&) = delete;

    virtual ~DrawAgent()
    {
        XFreePixmap(display, drawable_);
        XFreeGC(display, gc_);
    }

    size_t get_text_width(const std::string& text)
    {
        size_t width = 0;

        long ch;
        const char* text_buf = text.c_str();
        while (*text_buf)
        {
            size_t ch_len = utf8decode(text_buf, &ch, UTF_SIZE);
            size_t text_buf_delta = ch_len;

            // Pick a font.
            BarFont font = get_font_for_char(ch);

            width += font.getexts(text_buf, ch_len);
            text_buf += text_buf_delta;
        }

        return width;
    }

    int draw_text(const std::string& text,
                  const std::shared_ptr<const ColorScheme>& scheme,
                  int32_t x,
                  int32_t y,
                  uint32_t width,
                  uint32_t height,
                  uint32_t lpad,
                  bool invert)
    {
        XSetForeground(display, gc_, BACKGROUND_COLOR(scheme, invert).pixel);
        XFillRectangle(display, drawable_, gc_, x, y, width, height);
        XftDraw* draw = XftDrawCreate(display,
                                      drawable_,
                                      DefaultVisual(display, screen),
                                      DefaultColormap(display, screen));
        x += lpad;
        width -= lpad;

        long ch;
        const char* text_buf = text.c_str();
        while (*text_buf)
        {
            size_t ch_len = utf8decode(text_buf, &ch, UTF_SIZE);
            size_t text_buf_delta = ch_len;

            // Pick a font.
            BarFont font = get_font_for_char(ch);

            // y position where the character should be drawn.
            uint32_t ch_y =
                y + (height - font.get_height()) / 2 + font.get_xfont()->ascent;

            // Width of the character.
            uint32_t ch_width = font.getexts(text_buf, ch_len);
            // Shorten the character if it breaks past the planned width of the
            // text.
            while (ch_width > width && ch_len > 0)
            {
                ch_width = font.getexts(text_buf, ch_len);
                ch_len -= 1;
            }

            XftDrawStringUtf8(draw,
                              &FOREGROUND_COLOR(scheme, invert),
                              font.get_xfont(),
                              x,
                              ch_y,
                              (XftChar8*)text_buf,
                              ch_len);
            x += ch_width;
            width -= ch_width;
            text_buf += text_buf_delta;
        }

        XftDrawDestroy(draw);

        return x + width;
    }

    void draw_rectangle(const std::shared_ptr<const ColorScheme>& scheme,
                        int32_t x,
                        uint32_t y,
                        uint32_t width,
                        uint32_t height,
                        bool filled,
                        bool invert)
    {
        XSetForeground(display, gc_, FOREGROUND_COLOR(scheme, invert).pixel);
        if (filled)
        {
            XFillRectangle(display, drawable_, gc_, x, y, width, height);
        }
        else
        {
            XDrawRectangle(
                display, drawable_, gc_, x, y, width - 1, height - 1);
        }
    }

    void map(Window window,
             const int& x,
             const int& y,
             const unsigned int& width,
             const unsigned int& height)
    {
        XCopyArea(display, drawable_, window, gc_, x, y, width, height, 0, 0);
        XSync(display, True);
    }

private:
    DrawAgent()
        : screen_width_(sw)
        , screen_height_(sh)
    {
        drawable_ = XCreatePixmap(display,
                                  root,
                                  screen_width_,
                                  screen_height_,
                                  DefaultDepth(display, screen));
        gc_ = XCreateGC(display, root, 0, NULL);
        XSetLineAttributes(display, gc_, 1, LineSolid, CapButt, JoinMiter);
    }

    BarFont get_font_for_char(const long& ch)
    {
        BarFont ret;
        auto fonts = Palette::get_instance().get_fonts();
        auto it = std::find_if(fonts.begin(), fonts.end(), [&](BarFont& font) {
            return XftCharExists(display, font.get_xfont(), ch);
        });
        if (it != fonts.end())
        {
            ret = *it;
        }
        else
        {
            // Regardless of whether or not a fallback font is found, the
            // character must be drawn.
            ret = create_temporary_font(ch);
        }

        return ret;
    }

    BarFont create_temporary_font(const uint32_t& ch)
    {
        FcCharSet* fccharset = FcCharSetCreate();
        FcCharSetAddChar(fccharset, ch);
        XftResult result;

        auto first_font = Palette::get_instance().first_font();

        FcPattern* fcpattern = FcPatternDuplicate(first_font.get_pattern());
        FcPatternAddCharSet(fcpattern, FC_CHARSET, fccharset);
        FcPatternAddBool(fcpattern, FC_SCALABLE, FcTrue);
        FcConfigSubstitute(NULL, fcpattern, FcMatchPattern);
        FcDefaultSubstitute(fcpattern);

        FcPattern* match = XftFontMatch(display, screen, fcpattern, &result);
        FcCharSetDestroy(fccharset);
        FcPatternDestroy(fcpattern);

        BarFont font;
        if (match && font.create(match) &&
            XftCharExists(display, font.get_xfont(), ch))
        {
            return font;
        }
        else
        {
            // We have exhausted our options; go for the default font and hope
            // for the best.
            return Palette::get_instance().first_font();
        }
    }

    // The underlying drawable element.
    Drawable drawable_;

    // The width and height of the drawable area.
    uint32_t screen_width_;

    uint32_t screen_height_;

    // The graphics context.
    GC gc_;
};

size_t get_text_width(const std::string& text)
{
    return DrawAgent::get_instance().get_text_width(text);
}

int draw_text(const std::string& text,
              const std::shared_ptr<const ColorScheme>& scheme,
              int32_t x,
              int32_t y,
              uint32_t width,
              uint32_t height,
              uint32_t lpad,
              bool invert)
{
    return DrawAgent::get_instance().draw_text(
        text, scheme, x, y, width, height, lpad, invert);
}

void draw_rectangle(const std::shared_ptr<const ColorScheme>& scheme,
                    int32_t x,
                    uint32_t y,
                    uint32_t width,
                    uint32_t height,
                    bool filled,
                    bool invert)
{
    DrawAgent::get_instance().draw_rectangle(
        scheme, x, y, width, height, filled, invert);
}

void map_to_window(Window window,
                   const int& x,
                   const int& y,
                   const unsigned int& width,
                   const unsigned int& height)
{
    DrawAgent::get_instance().map(window, x, y, width, height);
}

}