#include "palette/ColorScheme.h"

ColorScheme::ColorScheme() {}

ColorScheme::ColorScheme(const char* fg, const char* bg, const char* bd)
{
    create_color(fg, &colors_[COLOR_FG]);
    create_color(bg, &colors_[COLOR_BG]);
    create_color(bd, &colors_[COLOR_BD]);
}

ColorScheme::ColorScheme(const ColorScheme& scm1, const ColorScheme& scm2)
{
    colors_[COLOR_FG] = scm2[COLOR_BG];
    colors_[COLOR_BG] = scm1[COLOR_BG];
    colors_[COLOR_BD] = scm1[COLOR_BD];
}

const Color& ColorScheme::operator[](const size_t& idx) const
{
    return colors_[idx];
}

bool ColorScheme::create_color(const char* clrname, Color* dest)
{
    if (!clrname)
    {
        return false;
    }

    bool ret = XftColorAllocName(display,
                                 DefaultVisual(display, screen),
                                 DefaultColormap(display, screen),
                                 clrname,
                                 dest);

    if (!ret)
    {
        printf("WARNING: cannot allocate color '%s'", clrname);
    }

    return ret;
}
