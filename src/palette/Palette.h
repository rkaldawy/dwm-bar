#ifndef PALETTE_H
#define PALETTE_H

#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "palette/ColorScheme.h"
#include "palette/Font.h"

/**
 * This is a singleton class which holds all the color schemes used by the bar.
 * Any other class can use this class to grab and use a color scheme.
 */

class Palette
{
public:
    static Palette& get_instance();

    Palette(const Palette&) = delete;

    Palette& operator=(const Palette&) = delete;

    void add_scheme(const std::string& name,
                    const std::string& fg,
                    const std::string& bg,
                    const std::string& bd);

    void add_color(const std::string& name, const char* color);

    std::shared_ptr<const ColorScheme> load_scheme(const std::string& name);

    const char* load_color(const std::string& name);

    std::shared_ptr<const ColorScheme> get_default_scheme();

    static std::shared_ptr<const ColorScheme> blend_colors(
        const std::shared_ptr<const ColorScheme>& scheme1,
        const std::shared_ptr<const ColorScheme>& scheme2);

    bool create_fontset(const std::vector<std::string>& names);

    const std::vector<BarFont>& get_fonts();

    BarFont first_font();

private:
    Palette();

    // List of available schemes for the drawables.
    std::map<const std::string, std::shared_ptr<const ColorScheme>> schemes_;

    // List of available colors which make up the schemes.
    std::map<const std::string, const char*> colors_;

    // Scheme to use if the map does not have a desired color.
    std::shared_ptr<const ColorScheme> default_scheme_;

    // Available fonts, in order of priority.
    std::vector<BarFont> fonts_;
};

#endif
