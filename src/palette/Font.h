#ifndef FONT_H
#define FONT_H

#include <cstdint>
#include <string>
#include <utility>

#include "util/XIncludes.h"

class BarFont
{
public:
    BarFont();

    virtual ~BarFont();

    bool create(const std::string& font_name);

    bool create(FcPattern* pattern);

    uint32_t getexts(const char* ch, size_t len);

    XftFont* get_xfont();

    FcPattern* get_pattern();

    uint32_t get_height();

private:
    XftFont* xfont_ = nullptr;

    FcPattern* pattern_ = nullptr;

    uint32_t height_;
};

#endif
