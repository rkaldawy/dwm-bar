#include "palette/Font.h"

BarFont::BarFont() {}

BarFont::~BarFont() {}

bool BarFont::create(const std::string& font_name)
{
    xfont_ = XftFontOpenName(display, screen, font_name.c_str());
    if (!xfont_)
    {
        fprintf(stderr,
                "error, cannot load font from name: '%s'\n",
                font_name.c_str());
        return false;
    }

    pattern_ = FcNameParse((FcChar8*)font_name.c_str());
    if (!pattern_)
    {
        fprintf(stderr,
                "error, cannot parse font name to pattern: '%s'\n",
                font_name.c_str());
        XftFontClose(display, xfont_);
        return false;
    }

    height_ = xfont_->ascent + xfont_->descent;

    return true;
}

bool BarFont::create(FcPattern* pattern)
{
    if (!pattern)
    {
        fprintf(stderr, "no font specified.\n");
        return false;
    }

    xfont_ = XftFontOpenPattern(display, pattern);
    if (!xfont_)
    {
        fprintf(stderr, "error, cannot load font from pattern.\n");
        return false;
    }

    pattern_ = pattern;
    height_ = xfont_->ascent + xfont_->descent;

    return true;
}

uint32_t BarFont::getexts(const char* ch, size_t len)
{
    XGlyphInfo ext;
    XftTextExtentsUtf8(display, xfont_, (XftChar8*)ch, len, &ext);

    return ext.xOff;
}

XftFont* BarFont::get_xfont()
{
    return xfont_;
}

FcPattern* BarFont::get_pattern()
{
    return pattern_;
}

uint32_t BarFont::get_height()
{
    return height_;
}