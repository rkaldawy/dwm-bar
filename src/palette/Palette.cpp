#include "palette/Palette.h"

Palette& Palette::get_instance()
{
    static Palette scheme_manager;
    return scheme_manager;
}

Palette::Palette()
{
    colors_["white"] = "#ffffff";
    colors_["black"] = "#000000";

    default_scheme_ =
        std::make_shared<ColorScheme>("#000000", "#ffffff", "#000000");
}

std::shared_ptr<const ColorScheme> Palette::get_default_scheme()
{
    return default_scheme_;
}

void Palette::add_scheme(const std::string& name,
                         const std::string& fg,
                         const std::string& bg,
                         const std::string& bd)
{
    auto scheme = std::make_shared<const ColorScheme>(
        load_color(fg), load_color(bg), load_color(bd));

    schemes_.emplace(name, scheme);
}

void Palette::add_color(const std::string& name, const char* color)
{
    colors_.emplace(name, color);
}

std::shared_ptr<const ColorScheme> Palette::load_scheme(const std::string& name)
{
    auto it = schemes_.find(name);
    if (it != schemes_.end())
    {
        return it->second;
    }
    else
    {
        return default_scheme_;
    }
}

const char* Palette::load_color(const std::string& name)
{
    auto it = colors_.find(name);
    if (it != colors_.end())
    {
        return it->second;
    }
    else
    {
        return "#000000";
    }
}

std::shared_ptr<const ColorScheme> Palette::blend_colors(
    const std::shared_ptr<const ColorScheme>& scheme1,
    const std::shared_ptr<const ColorScheme>& scheme2)
{
    return std::make_shared<const ColorScheme>(*scheme1, *scheme2);
}

bool Palette::create_fontset(const std::vector<std::string>& names)
{
    for (const auto& name : names)
    {
        BarFont font;
        if (font.create(name))
        {
            fonts_.push_back(font);
        }
    }
    return true;
}

const std::vector<BarFont>& Palette::get_fonts()
{
    return fonts_;
}

BarFont Palette::first_font()
{
    // if (fonts_.empty())
    //{
    //     return std::nullopt;
    // }

    return fonts_.at(0);
}