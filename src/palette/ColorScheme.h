#ifndef COLORSCHEME_H
#define COLORSCHEME_H

#include "util/XIncludes.h"

typedef XftColor Color;

const size_t COLOR_FG = 0;
const size_t COLOR_BG = 1;
const size_t COLOR_BD = 2;

class ColorScheme
{
public:
    ColorScheme();

    ColorScheme(const char* fg, const char* bg, const char* bd);

    ColorScheme(const ColorScheme& scm1, const ColorScheme& scm2);

    virtual ~ColorScheme() {}

    const Color& operator[](const size_t& idx) const;

    static bool create_color(const char* code, Color* dest);

private:
    Color colors_[3];
};

#endif
