#ifndef BAR_HPP
#define BAR_HPP

#include <errno.h>
#include <locale.h>
#include <pthread.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <map>
#include <memory>
#include <queue>
#include <vector>

#include "draw/DrawableElement.h"
#include "palette/Palette.h"
#include "units/Unit.h"
#include "util/DrawUtil.h"
#include "util/Util.h"

class Bar : public DrawableElt
{
public:
    Bar();

    virtual ~Bar() {}

    Bar(Bar& other);

    virtual void draw() override;

    virtual bool handle_button_event(XEvent* e) override;

    virtual std::vector<std::shared_ptr<DrawableElt>> get_child_elements()
        override;

    void register_units();

    size_t get_length();

    int get_x() { return x_; }

private:
    bool update_units();

    void update_window();

    void draw_bar();

    /* List of units to draw, from left to right. */
    std::vector<std::unique_ptr<Unit>> units_;
};

#endif
