#include "bar/Bar.h"
#include "manager/Manager.h"

#include "units/BatteryUnit.h"
#include "units/CpuUnit.h"
#include "units/DateUnit.h"
#include "units/KeyboardUnit.h"
#include "units/MemoryUnit.h"
#include "units/PowerUnit.h"
#include "units/TimeUnit.h"
#include "units/VolumeUnit.h"
#include "units/WifiUnit.h"

Bar::Bar()
    : DrawableElt("dwmbar")
{
    y_ = 0;
    auto font = Palette::get_instance().first_font();
    width_ = 0;
    height_ = font.get_height() + 2;
    left_pad_ = 2;
    right_pad_ = 6;

    default_scheme_ = Palette::get_instance().load_scheme("dblue");

    register_units();
    draw();

    should_draw_ = true;
    drawing_ = false;
    visible_ = true;
}

Bar::Bar(Bar& other)
    : DrawableElt(other.name_)
{
    for (auto& unit : other.units_)
    {
        units_.push_back(std::move(unit));
    }
    default_scheme_ = other.default_scheme_;
}

void Bar::register_units()
{
    // Load up some colors from the palette.
    auto lblue = Palette::get_instance().load_scheme("lblue");
    auto dblue = Palette::get_instance().load_scheme("dblue");
    auto dpink = Palette::get_instance().load_scheme("dpink");
    auto black = Palette::get_instance().load_scheme("black");
    auto lime = Palette::get_instance().load_scheme("lime");

    // temporary
    auto dblue2 = Palette::get_instance().load_scheme("dblue2");

    // Wifi unit.
    units_.push_back(std::make_unique<WifiUnit>(dblue2));

    // Keyboard language.
    units_.push_back(std::make_unique<KeyboardUnit>(lblue));

    // CPU usage unit.
    units_.push_back(std::make_unique<CpuUnit>(dblue, 5000000));

    // Memory usage unit.
    units_.push_back(std::make_unique<MemoryUnit>(lblue));

    // Volume unit.
    units_.push_back(std::make_unique<VolumeUnit>(dblue));

    // Battery unit.
    units_.push_back(std::make_unique<BatteryUnit>(lblue, 500000));

    // Date unit.
    units_.push_back(std::make_unique<DateUnit>(dblue));

    // Time unit.
    units_.push_back(std::make_unique<TimeUnit>(dpink));

    // Power unit.
    units_.push_back(std::make_unique<PowerUnit>(black, lime));
}

size_t Bar::get_length()
{
    return width_;
}

bool Bar::handle_button_event(XEvent* e)
{
    if (!visible_)
    {
        return false;
    }

    XButtonPressedEvent* ev = &e->xbutton;

    int pressed_x = ev->x_root;
    int pressed_y = ev->y_root;

    if (pressed_y > height_)
    {
        return false;
    }

    for (const auto& unit : units_)
    {
        if (pressed_x > unit->get_x() &&
            pressed_x < unit->get_x() + unit->get_width())
        {
            unit->handle_button_event(ev);
            return true;
        }
    }

    return false;
}

std::vector<std::shared_ptr<DrawableElt>> Bar::get_child_elements()
{
    std::vector<std::shared_ptr<DrawableElt>> ret;

    for (const auto& unit : units_)
    {
        const auto unit_drawables = unit->get_child_elements();
        ret.insert(ret.end(), unit_drawables.begin(), unit_drawables.end());
    }

    return ret;
}

void Bar::draw()
{
    x_ = monitor_width_;
    int width = 0;

    // Refresh the units, and check if anything needs to be redrawn.
    bool needs_redraw = false;
    for (auto rit = units_.rbegin(); rit != units_.rend(); rit++)
    {
        auto& unit = *rit;

        needs_redraw |= unit->update(x_);
        x_ -= unit->get_width();
        width += unit->get_width();
    }
    if (!needs_redraw && visible_)
    {
        return;
    }

    // Resize the bar, if necessary.
    if (!visible_)
    {
        width_ = width;
        instantiate_window();
    }
    else if (width_ != width)
    {
        width_ = width;
        resize_window();
    }

    // Redraw the bar.
    auto last_scheme = default_scheme_;
    for (auto& unit : units_)
    {
        auto edge_scheme = Palette::blend_colors(last_scheme, unit->scheme_);

        DrawUtils::draw_text(unit->leading_text_,
                             edge_scheme,
                             unit->get_x(),
                             y_,
                             unit->get_leading_text_width(),
                             height_,
                             unit->get_left_pad(),
                             0);

        DrawUtils::draw_text(unit->text_,
                             unit->scheme_,
                             unit->get_x() + unit->get_leading_text_width(),
                             y_,
                             unit->get_text_width(),
                             height_,
                             unit->get_left_pad(),
                             0);

        last_scheme = unit->scheme_;
    }
    DrawUtils::map_to_window(window_, x_, y_, sw, sh);

    // Once the bar is drawn, it is always visible.
    visible_ = true;
}
