#ifndef MENU_H
#define MENU_H

#include <functional>
#include <memory>
#include <string>

#include "palette/ColorScheme.h"
#include "palette/Palette.h"

class MenuItem
{
public:
    MenuItem(std::string icon,
             const std::shared_ptr<const ColorScheme>& scheme,
             std::function<void(void)> event,
             int top_pad = 0,
             int bottom_pad = 0,
             int left_pad = 2,
             int right_pad = 2)
        : icon_(icon)
        , scheme_(scheme)
        , event_(event)
        , top_pad_(top_pad)
        , bottom_pad_(bottom_pad)
        , left_pad_(left_pad)
        , right_pad_(right_pad)
    {
        icon_width_ = DrawUtils::get_text_width(icon);
        height_ = Palette::get_instance().first_font().get_height() + top_pad +
                  bottom_pad + 2;
    }

    virtual ~MenuItem() {}

    int get_width() const { return icon_width_ + left_pad_ + right_pad_; }

    int get_adjusted_left_pad(int width) const
    {
        int full_pad = (width - get_width()) / 2;
        return full_pad + left_pad_;
    }

    // Icon associated with the menu item.
    std::string icon_;

    // The item's color scheme.
    std::shared_ptr<const ColorScheme> scheme_;

    // Function when menu item is pressed.
    std::function<void(void)> event_;

    int y_;

    // The width of the icon.
    int icon_width_;

    // The menu item's height.
    int height_;

    // The padding at the top of a menu item.
    int top_pad_;

    // The padding at the bottom of a menu item.
    int bottom_pad_;

    // Left hand text padding for the item.
    int left_pad_;

    // Right hand text padding for the item.
    int right_pad_;
};

#endif