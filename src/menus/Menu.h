#ifndef MENU_HPP
#define MENU_HPP

#include <memory>
#include <string>
#include <vector>

#include "draw/DrawableElement.h"
#include "menus/MenuItem.h"
#include "palette/ColorScheme.h"

class Menu : public DrawableElt
{
public:
    Menu(const int& x,
         const int& y,
         const std::shared_ptr<const ColorScheme>& scheme,
         std::vector<MenuItem>& items,
         const int& between_pad = 2,
         const int& side_pad = 2);

    virtual ~Menu() {}

    virtual void draw() override;

    void resize(const int& x, const int& width);

    virtual bool handle_button_event(XEvent* e) override;

private:
    // The elements of the menu.
    std::vector<MenuItem> items_;

    std::shared_ptr<const ColorScheme> background_scheme_;

    int between_pad_;

    int side_pad_;
};

#endif
