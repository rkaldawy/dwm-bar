#include "menus/Menu.h"

Menu::Menu(const int& x,
           const int& y,
           const std::shared_ptr<const ColorScheme>& scheme,
           std::vector<MenuItem>& items,
           const int& between_pad,
           const int& side_pad)
    : DrawableElt("dwm_menu")
    , items_(std::move(items))
    , background_scheme_(scheme)
    , between_pad_(between_pad)
    , side_pad_(side_pad)
{
    x_ = x;
    y_ = y;
    default_scheme_ = scheme;
    width_ = 0;
    height_ = 0;

    for (auto& item : items_)
    {
        height_ += between_pad_;
        item.y_ = y_ + height_;
        height_ += item.height_;
        int item_width = item.get_width();
        width_ = item_width > width_ ? item_width : width_;
    }
    width_ += 2 * side_pad_;
    height_ += between_pad_;

    should_draw_ = false;
    drawing_ = false;
    visible_ = false;
}

void Menu::draw()
{
    if (!visible_)
    {
        window_ = construct_window(true);
    }

    // Draw the background of the menu.
    DrawUtils::draw_rectangle(
        background_scheme_, x_, y_, width_, height_, true, true);

    for (const auto& item : items_)
    {
        DrawUtils::draw_rectangle(item.scheme_,
                                  x_ + side_pad_,
                                  item.y_,
                                  width_ - 2 * side_pad_,
                                  item.height_,
                                  true,
                                  true);

        DrawUtils::draw_text(item.icon_,
                             item.scheme_,
                             x_ + side_pad_,
                             item.y_ + item.top_pad_,
                             width_ - 2 * side_pad_,
                             item.height_ - item.top_pad_,
                             item.get_adjusted_left_pad(width_ - 2 * side_pad_),
                             false);
    }

    DrawUtils::map_to_window(window_, x_, y_, width_, height_);

    visible_ = true;
}

void Menu::resize(const int& x, const int& width)
{
    x_ = x;
    width_ = width;
}

bool Menu::handle_button_event(XEvent* e)
{
    if (!visible_)
    {
        return false;
    }

    XButtonPressedEvent* ev = &e->xbutton;
    int pressed_x = ev->x_root;
    int pressed_y = ev->y_root;

    if (pressed_x < x_ || pressed_x > x_ + width_)
    {
        return false;
    }

    if (pressed_y < y_ || pressed_y > y_ + height_)
    {
        return false;
    }

    for (const auto& item : items_)
    {
        if (pressed_y > item.y_ && pressed_y < item.y_ + item.height_)
        {
            item.event_();
            should_draw_ = false;
            return true;
        }
    }
    return false;
}
